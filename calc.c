/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "stack.h"

struct stack *operands;
struct stack *operators;

/* perform cleanup in case of an invalid expression */
void invalid_expr(void)
{
	fputs("Error: invalid expression\n", stderr);
	free_stack(operands);
	free_stack(operators);
	exit(EXIT_FAILURE);
}

/* perform cleanup in case of a malloc() error*/
void malloc_err(void)
{
	fputs("Error: memory allocation failed\n", stderr);
	free_stack(operands);
	free_stack(operators);
	exit(EXIT_FAILURE);
}

/* check if given token is an operator */
bool is_operator(char token)
{
	return token == '+' || token == '-' || token == '*' || token == '/' ||
	       token == '^';
}

/* return precedence of given operator
 * precondition: is_operator(opr) */
int prec(char opr)
{
	assert(is_operator(opr));

	switch (opr) {
	case '+':
	case '-':
		return 2;
	case '*':
	case '/':
		return 3;
	case '^':
		return 4;
	default:
		/* this is never reached */
		return 0;
	}
}

/* apply mathematical operation to top two elements on the stack
 * precondition: operators->size >= 1 && operands->size >= 2 */
void eval(void)
{
	assert(operators->size >= 1 && operands->size >= 2);

	char opr = pop(operators).opr;
	float snd = pop(operands).opd;
	float fst = pop(operands).opd;

	assert(is_operator(opr));

	struct token result;
	result.type = TOKEN_TYPE_OPERAND;

	switch (opr) {
	case '+':
		result.opd = fst + snd;
		break;
	case '-':
		result.opd = fst - snd;
		break;
	case '*':
		result.opd = fst * snd;
		break;
	case '/':
		result.opd = fst / snd;
		break;
	case '^':
		result.opd = pow(fst, snd);
		break;
	default:
		/* this is never reached */
		break;
	}

	if (!push(operands, result))
		malloc_err();
}

/* remove leading whitespaces from string */
void rmspaces(char **str)
{
	while (isspace(**str))
		++*str;
}

/* return first token of given arithmetic expression
 * also check for validity */
struct token tokenize(char **expr, bool *opr_allowed, int *open_count)
{
	struct token token;
	if (*opr_allowed && is_operator(*expr[0])) {
		token.type = TOKEN_TYPE_OPERATOR;
		token.opr = *expr[0];
		++(*expr);
		*opr_allowed = false;
	} else if (!*opr_allowed && *expr[0] == '(') {
		++*open_count;
		token.type = TOKEN_TYPE_OPERATOR;
		token.opr = *expr[0];
		++(*expr);
	} else if (*opr_allowed && *expr[0] == ')') {
		if (*open_count < 1)
			invalid_expr();
		--*open_count;
		token.type = TOKEN_TYPE_OPERATOR;
		token.opr = *expr[0];
		++(*expr);
	} else {
		token.type = TOKEN_TYPE_OPERAND;
		char *rest;
		token.opd = strtof(*expr, &rest);
		if (*expr == rest || *opr_allowed)
			invalid_expr();
		*expr = rest;
		*opr_allowed = true;
	}

	rmspaces(expr);
	return token;
}

/* handle given token, which might be an operand or an operator */
void shunting_yard(struct token token)
{
	if (token.type == TOKEN_TYPE_OPERAND) {
		if (!push(operands, token))
			malloc_err();
	} else if (is_operator(token.opr)) {
		while (operators->size > 0 && peek(operators).opr != '(' &&
		       prec(peek(operators).opr) >= prec(token.opr)) {
			eval();
		}
		if (!push(operators, token))
			malloc_err();
	} else if (token.opr == '(') {
		if (!push(operators, token))
			malloc_err();
	} else if (token.opr == ')') {
		while (peek(operators).opr != '(') {
			assert(operators->size > 0);
			eval();
		}
		pop(operators);
	} else {
		/* this is never reached */
		fprintf(stderr, "Error: invalid operator '%c'\n", token.opr);
		exit(EXIT_FAILURE);
	}
}

/* calculate result of given expression */
float calc(char *expr)
{
	rmspaces(&expr);
	struct token token;
	bool opr_allowed = false;
	int open_count = 0;
	while (expr[0] != '\0') {
		token = tokenize(&expr, &opr_allowed, &open_count);
		shunting_yard(token);
	}
	if (open_count || !opr_allowed)
		invalid_expr();

	while (operators->size > 0)
		eval();
	if (operands->size != 1)
		invalid_expr();

	float result = peek(operands).opd;
	return result;
}

/* handle command line arguments */
int main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("Usage:   %s <arith_expr>\n"
		       "Example: %s \"5 + 2 * 3\"\n",
		       argv[0], argv[0]);
		return EXIT_FAILURE;
	}

	if (!init(&operands) || !init(&operators))
		malloc_err();

	float result = calc(argv[1]);
	printf("Result: %f\n", result);

	free_stack(operands);
	free_stack(operators);
	return EXIT_SUCCESS;
}

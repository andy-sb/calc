/* See LICENSE file for copyright and license details. */

#ifndef STACK_H
#define STACK_H

enum token_type {
	TOKEN_TYPE_OPERATOR,
	TOKEN_TYPE_OPERAND,
};

/* can store an operator or an operand */
struct token {
	enum token_type type;
	union {
		char opr;
		float opd;
	};
};

struct node {
	struct token data;
	struct node *next;
};

struct stack {
	struct node *head;
	int size;
};

/* initialize stack */
int init(struct stack **stk);

/* peek at top element in stack */
struct token peek(struct stack *stk);

/* push element onto stack */
int push(struct stack *stk, struct token data);

/* pop element from stack
 * precondition: stack is not empty */
struct token pop(struct stack *stk);

/* free all nodes in stack */
void free_stack(struct stack *stk);

#endif /* STACK_H */

/* See LICENSE file for copyright and license details. */

#include <assert.h>
#include <stdlib.h>

#include "stack.h"

/* initialize stack */
int init(struct stack **stk)
{
	*stk = malloc(sizeof **stk);
	if (!*stk)
		return 0;
	(*stk)->head = NULL;
	(*stk)->size = 0;
	return 1;
}

/* peek at top element in stack */
struct token peek(struct stack *stk)
{
	struct node *head = stk->head;
	return head->data;
}

/* push element onto stack */
int push(struct stack *stk, struct token data)
{
	struct node *new = malloc(sizeof *new);
	if (!new)
		return 0;
	new->data = data;
	new->next = stk->head;
	stk->head = new;
	++stk->size;
	return 1;
}

/* pop element from stack
 * precondition: stack is not empty */
struct token pop(struct stack *stk)
{
	assert(stk->head);

	struct node *tmp = stk->head;
	struct token data = tmp->data;
	stk->head = tmp->next;
	--stk->size;
	free(tmp);

	return data;
}

/* free all nodes in stack */
void free_stack(struct stack *stk)
{
	while (stk->head) {
		struct node *tmp = stk->head;
		stk->head = tmp->next;
		free(tmp);
	}
	free(stk);
}
